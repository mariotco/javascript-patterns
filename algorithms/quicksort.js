/*quicksort :D*/
var INDEX_SIZE = Math.pow(2,20);
var array = getRandomizedArray(INDEX_SIZE);//[2,8,3,9,4,1,5,0,2,6];
var startTime = new Date();

array = quicksort(array);
// later record end time
var endTime = new Date();
// time difference in ms
var timeDiff = endTime - startTime;
console.log(array);
console.log("Quicksort in JavaScript :D");
console.log("It took: " + timeDiff + " milliseconds to sort " + INDEX_SIZE + " elements");
console.log("Awesome!");



function quicksort(array){
	if(array.length<=1){return array;}
	if(allElementsAreEqual(array)){return array;}

	var pivot = 0;
	var pivotcounter = 0;
	var smallerArray = [];
	var greaterArray = [];
	var arrayLength = array.length;
	for (var i = 0; i <= arrayLength -1; i++) {
		if (array[i]<array[pivot]){
			smallerArray.push(array[i]);
		} else if(array[i]>array[pivot]) {
			greaterArray.push(array[i]);
		} else if(array[i]==array[pivot]) {
			pivotcounter++;
		}
	}
	return concatenate(quicksort(smallerArray), array[pivot], pivotcounter, quicksort(greaterArray));
}

function concatenate(smallerArray, pivotValue, pivotcounter, greaterArray){
	while(pivotcounter!=0){
		smallerArray.push(pivotValue)
		pivotcounter--;
	}
	return smallerArray.concat(greaterArray);
}

function allElementsAreEqual(array){
	var arrayLength = array.length;
	var firstValue = array[0]
	for (var i = 1; i <= arrayLength-1; i++) {
		if(firstValue!=array[i]){return false;}
	}
	return true;
}

function getRandomizedArray(size){
	var array = [];
	while(size != 0){
		array.push(Math.floor(Math.random() * 1000000));
		size--;
	}
	return array;
}